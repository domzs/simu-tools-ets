const {
 app, ipcMain, BrowserWindow, Menu 
} = require('electron');
const path = require('path');
const isDev = require('electron-is-dev');

require('electron-reload');

const remote = require('./background/remote/index');

let mainWindow;

const isMac = process.platform === 'darwin';

const createWindow = () => {
  mainWindow = new BrowserWindow({
    width: 800,
    height: 600,
    // frame: false,
    title: 'SimuTools',
    // resizable: false,
    center: true,
    maximizable: false,
    webPreferences: {
      nodeIntegration: true,
      webSecurity: false,
      // preload: __dirname + '/preload.js'
    },
  });

  const menu = Menu.buildFromTemplate([
    ...(isMac
      ? [
        {
          label: 'SimuTools',
          submenu: [
            { role: 'about' },
            { type: 'separator' },
            { role: 'services' },
            { type: 'separator' },
            { role: 'hide' },
            { role: 'hideothers' },
            { role: 'unhide' },
            { type: 'separator' },
            { role: 'quit' },
          ],
        },
      ]
      : []),
    // {
    //     label: 'File',
    //     submenu: [
    //         {label:'Adjust Notification Value'},
    //         {label:'CoinMarketCap'},
    //         {label:'Exit', click() {
    //           app.quit()
    //       } }
    //     ]
    // }
  ]);
  Menu.setApplicationMenu(menu);

  // mainWindow.loadURL(`file://${path.join(__dirname, '../public/index.html')}`)

  mainWindow.loadURL(
    isDev
      ? 'http://localhost:3000'
      : `file://${path.join(__dirname, '../build/index.html')}`,
  );

  mainWindow.on('closed', () => {
    mainWindow = null;
  });

  mainWindow.webContents.openDevTools();
};

app.on('ready', createWindow);

app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit();
  }
});

app.on('activate', () => {
  if (mainWindow === null) {
    createWindow();
  }
});

ipcMain.on('home-get-app-data', (event) => {
  event.reply('home-get-app-data', {
    osVersion: process.getSystemVersion(),
    nodeVersion: process.version,
    chromeVersion: process.versions.chrome,
    electronVersion: process.versions.electron,
    execPath: process.execPath,
    pid: process.pid,
  });
});

ipcMain.on('remote-get-host-gests', (event, arg) => {
  console.log(arg);
  remote.queryVMs(arg.ip, arg.queryCmd, (data) => {
    event.reply('remote-get-host-gests', {
      data,
    });
  });
});

ipcMain.on('remote-start-gest-remote', (event, arg) => {
  console.log(arg);
  remote.startGuestRemote(arg.ip, arg.moid, 'execVmrcCommand', (data) => {
    event.reply('remote-start-gest-remote', {
      data,
    });
  });
});
