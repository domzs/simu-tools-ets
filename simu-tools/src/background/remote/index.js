const { Client } = require('ssh2');
const chproc = require('child_process');
const util = require('util');
const os = require('os');

module.exports.queryVMs = (ip, queryCmd, callbackFn) => {
  console.log('Query ip:', ip, queryCmd);

  const conn = new Client();
  conn
    .on('ready', () => {
      console.log('Client :: ready');
      conn.exec(queryCmd, (err, stream) => {
        if (err) throw err;
        stream
          .on('close', (code, signal) => {
            console.log(
              'Stream :: close :: code: ' + code + ', signal: ' + signal,
            );
            conn.end();
          })
          .on('data', (data) => {
            console.log('STDOUT: \n' + data);

            const guests = [];

            const lines = data.toString().split('\n');

            lines.forEach((line) => {
              if (line.length === 0) {
                return;
              }

              // delete more then one space and split by ' '
              const guestData = line
                .replace(/^\s+|\s+$|\s+(?=\s)/g, '')
                .split(' ');

              // first line is a header: Vmid Name File Guest_OS Version Annotation
              if (isNaN(guestData[0])) {
                return;
              }

              guests.push({ name: guestData[1], moid: guestData[0] });
            });

            callbackFn(guests);
          })
          .stderr.on('data', (data) => {
            console.log('STDERR: ' + data);
          });
      });
    })
    .on('keyboard-interactive', (name, instructions, lang, prompts, cb) => {
      if (~prompts[0].prompt.indexOf('Password:')) cb(['M@n@g3r']);
    })
    .on('error', (err) => {
      console.log('SSH2 Error: ', err);
    })
    .connect({
      host: ip,
      port: 22,
      username: 'root',
      // readyTimeout: 99999,
      tryKeyboard: true,
      // debug: console.log,
      // readyTimeout : 60000,
      // keepaliveInterval : 600000
      // privateKey: require('fs').readFileSync('/Users/domzs/.ssh/id_rsa')
    });

  // const host = _.find(remoteCfg.hosts, h => h.ip === ip);
  // console.log(`queryVMs: ${host.name} (${ip})`);
};

module.exports.startGuestRemote = (ip, moid, execVmrcCmd, callbackFn) => {
  chproc.exec(
    util.format(
      // execVmrcCmd
      'open /Applications/VMware\\ Remote\\ Console.app vmrc://root@%s/?moid=%s',
      ip,
      moid,
    ),
    (err /* , stdout, stderr */) => {
      if (err !== null) {
        callbackFn(err.message);
        return;
      }

      callbackFn('OK');
    },
  );
};
