/* global localStorage */
import { defaultRemoteConfig } from '../config/remote';

const KEY_NAME = 'RemoteConfig';

const getStoreItem = (key: string): string | null => {
  const browserStorage = typeof localStorage === 'undefined' ? null : localStorage;

  if (browserStorage) {
    const storeItem = browserStorage.getItem(key);
    return storeItem;
  }

  return null;
};

export const getRemoteConfig = (): RemoteConfig => {
  const storeItem = getStoreItem(KEY_NAME);

  if (storeItem) {
    const remoteConfig: RemoteConfig = JSON.parse(storeItem);
    return remoteConfig;
  }

  return defaultRemoteConfig;
};

export const setRemoteConfig = (remoteConfig: RemoteConfig): void => {
  localStorage.setItem(KEY_NAME, JSON.stringify(remoteConfig));
};
