// type ChildrenProps = {
//     children: {
//         content: ReactNode
//         header?: ReactNode
//         media?: ReactNode
//         actions?: ReactNode
//     }
// }

interface RemoteConfig {
    execQueryVMsCmd: string;
    execVmrcCmds: {
      darwin: string;
      win32: string;
      linux: string;
    };

    hosts: Host[];
}

interface Host {
    name: string;
    ip: string;
}

interface RemoteContextInterface {
  config: RemoteConfig;
  setConfig(remoteConfig: RemoteConfig): void;
}

type RemoteProps = {
  remote: RemoteContextInterface;
}

interface Guest {
  name: string;
  moid: string;
}

interface RemoteQeuryGuests {
  data: Guest[];
}
