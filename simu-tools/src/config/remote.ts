export const defaultRemoteConfig: RemoteConfig = {
  execQueryVMsCmd: '/sbin/vim-cmd vmsvc/getallvms',
  execVmrcCmds: {
    darwin: 'open /Applications/VMware\\ Remote\\ Console.app vmrc://root@%s/?moid=%s',
    win32: '',
    linux: '',
  },

  hosts: [
    {
      name: 'VERONA-1',
      ip: '10.5.191.34',
    },
    {
      name: 'VERONA-2',
      ip: '10.5.191.35',
    },
    {
      name: 'BSZG-1',
      ip: '10.5.191.36',
    },
    {
      name: 'BSZG-2',
      ip: '10.5.191.37',
    },
    {
      name: 'BSZG-3',
      ip: '10.5.191.38',
    },
    {
      name: 'BSZG-4',
      ip: '10.5.191.39',
    },
  ],
};

export const dummy = {};
