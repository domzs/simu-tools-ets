/* global window */
import React, { useState, useEffect } from 'react';
import pjson from '../../../package.json';

const { ipcRenderer } = window.require('electron');

interface AppInfo {
  appVersion: string;
  osVersion: string;
  nodeVersion: string;
  chromeVersion: string;
  electronVersion: string;
  execPath: string;
  pid: number;
}

const Home = (): JSX.Element => {
  const [appInfo, setAppInfo] = useState<AppInfo | null>(null);

  useEffect(() => {
    ipcRenderer.send('home-get-app-data', '');
  }, []);

  ipcRenderer.on('home-get-app-data', (event: Electron.Event, arg: AppInfo) => {
    // Response
    setAppInfo({ appVersion: pjson.version, ...arg });
  });

  if (appInfo === null) {
    return (<div>Loading...</div>);
  }

  //   console.log(appInfo!.nodeVersion);

  return (
    <div>
      <h1 className="bp3-heading">SimuTools</h1>
      <div className="bp3-monospace-text">
        App. verzió:
        {' '}
        <span className="bp3-text-disabled">{appInfo.appVersion}</span>
      </div>
      <div className="bp3-monospace-text">
        OS verzió:
        {' '}
        <span className="bp3-text-disabled">{appInfo.osVersion}</span>
      </div>
      <div className="bp3-monospace-text">
        Node verzió:
        {' '}
        <span className="bp3-text-disabled">{appInfo.nodeVersion}</span>
      </div>
      <div className="bp3-monospace-text">
        Chrome verzió:
        {' '}
        <span className="bp3-text-disabled">{appInfo.chromeVersion}</span>
      </div>
      <div className="bp3-monospace-text">
        Electron verzió:
        {' '}
        <span className="bp3-text-disabled">{appInfo.electronVersion}</span>
      </div>
      <div className="bp3-monospace-text">
        Process ID:
        {' '}
        <span className="bp3-text-disabled">{appInfo.pid}</span>
      </div>
      <div className="bp3-monospace-text">
        Exec path:
        {' '}
        <span className="bp3-text-disabled">{appInfo.execPath}</span>
      </div>
    </div>
  );
};

export default React.memo(Home);
