import React from 'react';
import classnames from 'classnames';

import './App.css';
import Layout from "../Layout";

const App: React.FC = () => {
  return (
    <div className={classnames('App', 'bp3-dark')}>
      <Layout />
    </div>
    // <div className="App">
    //   <header className="App-header">
    //     <img src={logo} className="App-logo" alt="logo" />
    //     <p>
    //       Edit <code>src/App.tsx</code> and save to reload.
    //     </p>
    //     <a
    //       className="App-link"
    //       href="https://reactjs.org"
    //       target="_blank"
    //       rel="noopener noreferrer"
    //     >
    //       Learn React
    //     </a>
    //   </header>
    // </div>
  );
}

export default App;
