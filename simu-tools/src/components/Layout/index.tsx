import React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';

import Sidebar from './Sidebar';
import Home from '../Home';
import Remote from '../Remote';
import SySwitch from '../SySwitch';

const Layout: React.FC = () => (
  <>
    <Router>
      <div style={{ display: 'flex' }}>
        <Sidebar />
        <div style={{ margin: 20, display: 'table', width: '100%' }}>
          <Switch>
            <Route exact path="/">
              <Home />
            </Route>
            <Route path="/remote">
              <Remote />
            </Route>
            <Route path="/syswitch">
              <SySwitch />
            </Route>
          </Switch>
        </div>
      </div>
    </Router>
  </>
);

export default Layout;
