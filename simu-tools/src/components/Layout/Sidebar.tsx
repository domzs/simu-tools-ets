import React from 'react';
import { withRouter } from 'react-router';
import { RouteComponentProps, Link } from 'react-router-dom';

import {
  Classes, Icon, Menu, MenuItem, Tooltip, Position,
} from '@blueprintjs/core';

const ICON_SIZE = 60;

const toolMenuData = [
  {
    path: '/',
    name: 'Home',
    icon: <Icon icon="home" iconSize={ICON_SIZE} />,
  },
  {
    path: '/remote',
    name: 'Remote',
    icon: <Icon icon="desktop" iconSize={ICON_SIZE} />,
  },
  {
    path: '/syswitch',
    name: 'SySwitch',
    icon: <Icon icon="random" iconSize={ICON_SIZE} />,
  },
];

const Sidebar = ({ location }: RouteComponentProps): JSX.Element => {
  const renderToolMenuItems = (): JSX.Element[] => toolMenuData.map((item) => (
    <Tooltip key={item.name} content={item.name} position={Position.RIGHT}>
      <Link to={item.path} style={{ outline: 'none' }}>
        <MenuItem tagName="div" active={(item.path === '/' && location.pathname === item.path) || (item.path !== '/' && location.pathname.startsWith(item.path))} icon={item.icon} />
      </Link>
    </Tooltip>
  ));

  return (
    <div style={{ height: '100vh' }}>
      <Menu className={Classes.ELEVATION_1} style={{ minWidth: 91, maxWidth: 91, height: '100%' }}>
        {renderToolMenuItems()}
      </Menu>
    </div>
  );
};

export default withRouter(Sidebar);
