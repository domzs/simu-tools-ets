import React, { useState } from 'react';
import { useHistory } from 'react-router-dom';
import {
  FormGroup, InputGroup, Divider, Button,
} from '@blueprintjs/core';
import { withRemoteContext } from '../../contexts/RemoteContext';

const Settings = ({ remote }: RemoteProps): JSX.Element => {
  const history = useHistory();
  const [config, setConfig] = useState<RemoteConfig>(remote.config);

  const handleChange = (event: React.FormEvent<HTMLInputElement>): void => {
    if (event.currentTarget.id === 'execQueryVMsCmd') {
      setConfig({ ...config, execQueryVMsCmd: event.currentTarget.value });
    } else if (event.currentTarget.id === 'execVmrcCmdsWindows') {
      setConfig({ ...config, execVmrcCmds: { ...config.execVmrcCmds, win32: event.currentTarget.value } });
    } else if (event.currentTarget.id === 'execVmrcCmdsLinux') {
      setConfig({ ...config, execVmrcCmds: { ...config.execVmrcCmds, linux: event.currentTarget.value } });
    } else if (event.currentTarget.id === 'execVmrcCmdsDarwin') {
      setConfig({ ...config, execVmrcCmds: { ...config.execVmrcCmds, darwin: event.currentTarget.value } });
    }
  };

  const handleCancel = (): void => {
    history.push('/remote');
  };

  const handleSave = (): void => {
    remote.setConfig(config);
    history.push('/remote');
  };

  return (
    <div style={{ marginTop: 15 }}>

      <FormGroup
        // helperText="A VMWare host gépen a guest gépek lekérdezését végző VIM-CLI parancs"
        label="Guest query CLI command"
        labelFor="execQueryVMsCmd"
        labelInfo="(kötelező)"
      >
        <InputGroup id="execQueryVMsCmd" placeholder="Exec query VMs CLI command" onChange={handleChange} value={config.execQueryVMsCmd} />
      </FormGroup>

      <Divider />

      <FormGroup
        // helperText="A távoli asztal kapcsolatot indító parancs"
        label="Windows távoli asztal parancs"
        labelFor="execVmrcCmdsWindows"
      // labelInfo="(kötelező)"
      >
        <InputGroup id="execVmrcCmdsWindows" placeholder="Windows távoli asztal parancs" onChange={handleChange} value={config.execVmrcCmds.win32} />
      </FormGroup>

      <FormGroup
        // helperText=""
        label="Linux távoli asztal parancs"
        labelFor="execVmrcCmdsLinux"
      // labelInfo="(kötelező)"
      >
        <InputGroup id="execVmrcCmdsLinux" placeholder="Linux távoli asztal parancs" onChange={handleChange} value={config.execVmrcCmds.linux} />
      </FormGroup>


      <FormGroup
        // helperText=""
        label="MacOS távoli asztal parancs"
        labelFor="execVmrcCmdsDarwin"
      // labelInfo="(kötelező)"
      >
        <InputGroup id="execVmrcCmdsDarwin" placeholder="MacOS távoli asztal parancs" onChange={handleChange} value={config.execVmrcCmds.darwin} />
      </FormGroup>

      <div style={{ display: 'flex', justifyContent: 'flex-end' }}>
        <Button style={{ marginRight: 10 }} text="Mégsem" onClick={handleCancel} />
        <Button text="Mentés" onClick={handleSave} />
      </div>
    </div>
  );
};

export default withRemoteContext(Settings);
