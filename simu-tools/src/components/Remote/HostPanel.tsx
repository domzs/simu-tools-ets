/* global window */
import React, { useState, useEffect } from 'react';
import {
  Spinner, Icon, Callout,
} from '@blueprintjs/core';

const { ipcRenderer } = window.require('electron');

type HostPanelProps = {
  active: boolean;
  ip: string;
  queryCmd: string;
}

const HostPanel = ({
  active, ip, queryCmd,
}: HostPanelProps): JSX.Element => {
  const [guests, setGuests] = useState<Guest[] | null>(null);

  useEffect(() => {
    if (active && (guests === null || guests === [])) {
      ipcRenderer.on('remote-get-host-gests', (event: Electron.Event, arg: RemoteQeuryGuests) => {
        // console.log(arg);
        setGuests(arg.data);
      });

      ipcRenderer.send('remote-get-host-gests', { ip, queryCmd });
      return (): void => { ipcRenderer.removeAllListeners('remote-get-host-gests'); };
    }
  }, [active, ip, queryCmd, guests]);

  if (guests === null) {
    return <Spinner size={50} />;
  }

  const handleStartRemote = (moid: string): void => {
    // console.log(ip, moid);
    ipcRenderer.on('remote-start-gest-remote', (event: Electron.Event, arg: any) => {
      ipcRenderer.removeAllListeners('remote-start-gest-remote');
      console.log(arg);
    });

    ipcRenderer.send('remote-start-gest-remote', { ip, moid });
  };

  return (
    <div style={{ display: 'flex', alignItems: 'flex-start', flexWrap: 'wrap' }}>
      {
        guests.map((guest: Guest) => (
          <Callout
            key={guest.moid}
            title={`${guest.name} (${guest.moid})`}
            // icon={IconNames.DESKTOP}
            icon={<Icon icon="desktop" onClick={() => handleStartRemote(guest.moid)} style={{ cursor: 'pointer' }} />}
            style={{ marginBottom: 5, width: '100%' }}
          />
        ))
      }
    </div>
  );
};


export default React.memo<HostPanelProps>(HostPanel);
