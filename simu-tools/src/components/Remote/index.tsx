import React, { useState } from 'react';
import { Switch, Route } from 'react-router-dom';
import { Tab, Tabs, TabId } from '@blueprintjs/core';

import { RemoteProvider, withRemoteContext } from '../../contexts/RemoteContext';
import HostPanel from './HostPanel';
import NavBar from './NavBar';
import Settings from './Settings';

const Remote = ({ remote }: RemoteProps): JSX.Element => {
  const [seletedHostTab, setSelectedHostTab] = useState<TabId>(remote.config.hosts[0].name);

  const renderHostTabs = (): JSX.Element | JSX.Element[] => remote.config.hosts.map((host: Host) => <Tab key={host.name} id={host.name} title={host.name} style={{ outline: 'none' }} panel={<HostPanel active={seletedHostTab === host.name} ip={host.ip} queryCmd={remote.config.execQueryVMsCmd} />} />);

  const onTabChange = (newTabId: TabId): void => {
    setSelectedHostTab(newTabId);
  };

  return (
    <RemoteProvider>
      <div style={{ display: 'table', width: '100%' }}>
        <NavBar />
        <Switch>
          <Route exact path="/remote">
            <div style={{ marginTop: 15 }}>
              <Tabs
                id="remoteTabs"
                selectedTabId={seletedHostTab}
                // renderActiveTabPanelOnly
                vertical
                onChange={onTabChange}
              >
                {renderHostTabs()}
              </Tabs>
            </div>
          </Route>
          <Route exact path="/remote/settings">
            <Settings />
          </Route>
        </Switch>
      </div>
    </RemoteProvider>
  );
};

export default withRemoteContext(Remote);
