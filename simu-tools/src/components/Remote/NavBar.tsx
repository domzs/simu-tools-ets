import React from 'react';
import { Link } from 'react-router-dom';
import {
  Navbar, Tooltip, Button, Alignment, Position,
} from '@blueprintjs/core';

const NavPanel = (): JSX.Element => (
  <Navbar style={{ width: '100%' }}>
    <Navbar.Group align={Alignment.LEFT}>
      <Navbar.Heading>Remote</Navbar.Heading>
      <Navbar.Divider />
    </Navbar.Group>
    <Navbar.Group align={Alignment.RIGHT}>
      <Tooltip content="Újratöltés" position={Position.BOTTOM}>
        <Button className="bp3-minimal" icon="refresh" />
      </Tooltip>
      <Link to="/remote/settings">
        <Tooltip content="Beállítások" position={Position.BOTTOM}>
          <Button className="bp3-minimal" icon="settings" />
        </Tooltip>
      </Link>
    </Navbar.Group>
  </Navbar>
);

export default React.memo(NavPanel);
