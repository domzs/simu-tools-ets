import React, { useState } from 'react';

import { getRemoteConfig, setRemoteConfig } from '../utils/remoteConfig';

type RemoteProviderProps = {
  children: JSX.Element | JSX.Element[];
}

export const RemoteContext = React.createContext<RemoteContextInterface>({
  config: getRemoteConfig(),
  setConfig: () => { },
});
RemoteContext.displayName = 'RemoteContext';

export const RemoteProvider = ({ children }: RemoteProviderProps): JSX.Element => {
  const [state, setState] = useState(getRemoteConfig());

  const setConfig = (remoteConfig: RemoteConfig): void => {
    setState(remoteConfig);
    setRemoteConfig(remoteConfig);
  };

  return (
    <RemoteContext.Provider
      value={{
        config: state,
        setConfig,
      }}
    >
      {children}
    </RemoteContext.Provider>
  );
};
RemoteProvider.displayName = 'RemoteProvider';

export const withRemoteContext = (Component: React.ReactType) => (): JSX.Element => (
  <RemoteContext.Consumer>
    {(remoteDataProps): JSX.Element => (<Component remote={remoteDataProps} />)}
  </RemoteContext.Consumer>
);
